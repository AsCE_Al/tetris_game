﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

	float tempTime;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		Vector3 position = this.transform.position;
		if (position.y > -6)
		{
			tempTime += Time.deltaTime;
			if (tempTime > 1) {
				position.y--;
				tempTime = 0;
			}

			if (Input.GetKeyDown(KeyCode.LeftArrow)) {
				position.x--;
			} else if (Input.GetKeyDown(KeyCode.RightArrow)) {
				position.x++;
			} else if (Input.GetKeyDown(KeyCode.DownArrow)) {
				position.y--;
			} else if (Input.GetKeyDown(KeyCode.UpArrow)) {
				this.transform.Rotate(0, 0, 90, Space.World);
			}

			this.transform.position = position;
		}
	}
}